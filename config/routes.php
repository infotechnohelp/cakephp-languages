<?php

use Cake\Routing\Router;
use Languages\Lib\LocaleManager;
use Cake\Core\Configure;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Route\DashedRoute;

$routerCallbacks = Configure::read('routerCallbacks');

$requiredRouterCallbacks = [
    $routerCallbacks['project'],
    $routerCallbacks['cakephp-core'],
];

foreach ($requiredRouterCallbacks as $routerCallback) {
    foreach (LocaleManager::getLanguageCodePrefixes() as $language) {
        Router::scope('/' . $language, ['language' => $language], $routerCallback);
    }
}

Router::plugin(
    'Languages',
    ['path' => '/languages'],
    function (RouteBuilder $routes) {
        $routes->prefix('api', function (RouteBuilder $routes) {
            $routes->fallbacks(DashedRoute::class);
        });
        $routes->fallbacks(DashedRoute::class);
    }
);