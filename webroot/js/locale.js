function Locale(phrasesJson) {
    this.phrases = JSON.parse(phrasesJson);
}

Locale.prototype.translate = function (key) {
    return this.phrases[key];
};